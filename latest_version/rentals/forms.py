from django import forms
from .models import Car, Newaccount, Customer, Order, Store
import datetime
from django.forms.widgets import SelectDateWidget

class Search(forms.Form):
    LOWERBUDGET = forms.IntegerField(min_value = 0, label = 'Min price', required=False)
    UPPERBUDGET = forms.IntegerField(min_value = 0, label = 'Max price', required=False)
    CAR_MAKENAME_CHOICES = (
        ('BMW', 'BMW'),
        ('MAZDA', 'MAZDA'),
        ('ALFA ROMEO', 'ALFA ROMEO'),
        ('VOLKSWAGEN', 'VOLKSWAGEN'),
        ('MERCEDES-BENZ', 'MERCEDES-BENZ'),
        ('DATSUN', 'DATSUN'),
        ('RENAULT', 'RENAULT'),
        ('VOLVO', 'VOLVO'),
        ('NISSAN', 'NISSAN'),
        ('PEUGEOT', 'PEUGEOT'),
        ('EUNOS', 'EUNOS'),
        ('MITSUBISHI', 'MITSUBISHI'),
        ('LAND ROVER', 'LAND ROVER'),
        ('CHRYSLER', 'CHRYSLER'),
        ('TOYOTA', 'TOYOTA'),
        ('AUDI', 'AUDI'),
        ('SAAB', 'SAAB'),
        ('HONDA', 'HONDA')
    )
    CAR_MAKENAME = forms.ChoiceField(label = 'Brand', choices = CAR_MAKENAME_CHOICES, required=False)

class Register(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, label = 'Password (*)')
    repassword = forms.CharField(widget=forms.PasswordInput, label='Confirm Password (*)')
    dob = forms.DateField(widget=SelectDateWidget(years=range(1930, 2018)))
    class Meta:
        model = Newaccount
        
        #Change labels
        labels = {
            "user_name": "Username (*)",
            "email": "Email address (*)",
            "phone_number": "Phone number (*)",
            "stress_address": "Street address (*)",
            "user_city": "City (*)",
            "dob": "D.O.B (*)",
        }
        fields = ['user_name', 'password', 'repassword', 'email', 'phone_number', 'stress_address', 'user_city', 'dob']

class Login(forms.Form):
    user_name = forms.CharField(label = 'Username')
    password = forms.CharField(widget=forms.PasswordInput, label = 'Password')

class Edit_Profile(forms.Form):
    email = forms.EmailField(required=False,label = 'Email address')
    phone_number = forms.CharField(required=False, label = 'Phone number')
    dob = forms.DateField(required=False, widget=SelectDateWidget(years=range(1930, 2018)), label = 'D.O.B')
    stress_address = forms.CharField(required=False, label = 'Street address')
    user_city = forms.CharField(required=False, label = 'City')

class History(forms.ModelForm):
    CUSTOMERHISTORY = forms.IntegerField(min_value = 0, label = 'Customer ID')

    
class NewCustomer(forms.ModelForm):
    customer_birthday = forms.DateField(widget=SelectDateWidget(years=range(1930, 2018)))
    class Meta:
        model = Customer
        
        #Change labels
        labels = {
            "customer_name": "Name  (*)",
            "customer_phone": "Phone number  (*)",
            "customer_address": "Address  (*)",
            "customer_birthday": "D.O.B  (*)",
            "customer_occupation": "Occupation (*)",
            "customer_gender": "Gender  (*)",
        }
        fields = ['customer_name', 'customer_phone', 'customer_address', 'customer_birthday', 'customer_occupation', 'customer_gender']    
        
class Pickup(forms.ModelForm):
#    order_returnstore = forms.ModelChoiceField(queryset=Store.objects.all(), required=False)
#    order_returndate = forms.DateField(required=False)
    class Meta:
        model = Order
        
        #Change labels
        labels = {
            "order_pickupstore": "Pickup Store ID (*)",
            "car": "Car ID (*)",
            "customer": "Customer ID (*)",

        }
        fields = ['customer', 'car', 'order_pickupstore']       
        

       
class ReturnCar(forms.Form):
    order_id = forms.IntegerField(label='Order ID (*)')
    store_id = forms.IntegerField(label='Store ID (*)')   