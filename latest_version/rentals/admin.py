from django.contrib import admin
from django.shortcuts import redirect

## Register your models here.
from .models import Car
from .models import Customer
from .models import Store
from .models import Order



class CustomerAdmin(admin.ModelAdmin):
    def response_add(self, request, obj, post_url_continue=None):
        return redirect('/edit_customer/')

    def response_change(self, request, obj, post_url_continue=None):
        return redirect('/edit_customer/')

class CarAdmin(admin.ModelAdmin):
    def response_add(self, request, obj, post_url_continue=None):
        return redirect('/edit_car/')

    def response_change(self, request, obj, post_url_continue=None):
        return redirect('/edit_car/')
    
class OrderAdmin(admin.ModelAdmin):
    def response_add(self, request, obj, post_url_continue=None):
        return redirect('/edit_order/')

    def response_change(self, request, obj, post_url_continue=None):
        return redirect('/edit_order/')
    
class StoreAdmin(admin.ModelAdmin):
    def response_add(self, request, obj, post_url_continue=None):
        return redirect('/edit_store/')

    def response_change(self, request, obj, post_url_continue=None):
        return redirect('/edit_store/')
    
admin.site.register(Car,CarAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Store, StoreAdmin)
admin.site.register(Order, OrderAdmin)
