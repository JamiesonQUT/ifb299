import hashlib

from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import Http404
from django.core.mail import send_mail
from django.conf import settings 
from django.shortcuts import redirect
from django.db.models import Count
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q

import random
import datetime
from django.http import HttpResponseRedirect
from django.contrib import messages

from .models import Car, Newaccount
from .models import Customer
from .models import Store
from .models import Order
from .forms import Search, Register, Login, Edit_Profile, NewCustomer, Pickup, ReturnCar


def index(request):
    form = Search()
    season = 0
    month = datetime.datetime.today().month
    if month in (1, 2, 3):
        most_rented = Order.objects.filter(Q(order_pickupdate__month= 1)|Q(order_pickupdate__month= 2)|Q(order_pickupdate__month= 3)).values_list('car').annotate(car_count=Count('car')).order_by('-car_count')[:6]
        season = 'winter'
    elif month in (4, 5, 6):
        most_rented = Order.objects.filter(Q(order_pickupdate__month= 4)|Q(order_pickupdate__month= 5)|Q(order_pickupdate__month= 6)).values_list('car').annotate(car_count=Count('car')).order_by('-car_count')[:6]
        season = 'spring'
    elif month in (7, 8, 9):
        most_rented = Order.objects.filter(Q(order_pickupdate__month= 7)|Q(order_pickupdate__month= 8)|Q(order_pickupdate__month= 9)).values_list('car').annotate(car_count=Count('car')).order_by('-car_count')[:6]
        season = 'summer'
    else:
        most_rented = Order.objects.filter(Q(order_pickupdate__month= 10)|Q(order_pickupdate__month= 11)|Q(order_pickupdate__month= 12)).values_list('car').annotate(car_count=Count('car')).order_by('-car_count')[:6]
        season = 'autumn'
    Qdata = list(most_rented)
    IDs = [list(elem) for elem in Qdata]
    for x in range(6):
        del IDs[x][1]
    IDs = sum(IDs, [])
    cars = Car.objects.filter(pk__in=IDs)
    if (request.user.is_superuser):
        return render(request, 'rentals/staff.html', {'stafforcust': 'Customer Site'})
    if(request.user.is_authenticated):
        return render(request, 'rentals/user_index.html', {'IDs':IDs,'most_rented':most_rented,'cars': cars, 'form': form})

    return render(request, 'rentals/index.html', {'IDs':IDs,'most_rented':most_rented,'cars': cars, 'form': form, 'tag1': 'Login', 'tag2':'Register'})
	
def search(request):
    if request.method == 'POST':
        form = Search(request.POST)
        lower = request.POST.get('LOWERBUDGET')
        upper = request.POST.get('UPPERBUDGET')
        brand = request.POST.get('CAR_MAKENAME', None)
        question_set = Car.objects.filter()
        if lower:
            question_set = question_set.filter(car_pricenew__gte = lower)
        if upper:
            question_set = question_set.filter(car_pricenew__lte = upper)
        if brand:
            question_set = question_set.filter(car_makename__contains = brand)

        return render(request, 'rentals/search.html', { 'searched': question_set, 'form': form, 'tag1': 'Login', 'tag2':'Register'})

    else:
        form = Search()
    return render(request, 'rentals/search.html', {'form': form})

def user_search(request):
    if request.method == 'POST':
        result = ''
        form = Search(request.POST)
        lower = request.POST.get('LOWERBUDGET')
        upper = request.POST.get('UPPERBUDGET')
        brand = request.POST.get('CAR_MAKENAME', None)
        question_set = Car.objects.filter()

        if lower:
            question_set = question_set.filter(car_pricenew__gte = lower)
        if upper:
            question_set = question_set.filter(car_pricenew__lte = upper)
        if brand:
            question_set = question_set.filter(car_makename__contains = brand)
        if not question_set:
            result = 'No car meets your requirement'


        return render(request, 'rentals/user_search.html', { 'searched': question_set, 'form': form, 'tag1': 'My account', 'tag2':'Logout', 'result': result})

    else:
        form = Search()
    return render(request, 'rentals/user_search.html', {'form': form, 'tag1': 'My account', 'tag2':'Logout'})
def create_account(request):
    form = Register()
    if request.method == 'POST':
        form = Register(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            name = data['user_name']
            rawPassword = data['password']
            password = hashlib.md5(rawPassword.encode('utf-8')).hexdigest()
            comfirmPass = data['repassword']
            email = data['email']
            phoneNumber = data['phone_number']
            stress_address = data['stress_address']
            city = data['user_city']
            dob = data['dob']
            try:
                if Newaccount.objects.filter(user_name=name).exists():
                    raise ValueError('please choose another user_name')
                if rawPassword != comfirmPass:
                    raise ValueError('Passwords do not match!!!')
                if len(str(phoneNumber)) >= 11:
                    raise ValueError('Invalid Phone number! Must be less than 11 digits')
            except ValueError as error:
                context = {
                    "form": form,
                    "error_message": error.args[0],
                }
                return render(request, 'rentals/create_account.html', context)
            else:
                new_user = Newaccount(user_name = name, password = password, email = email, phone_number = phoneNumber, stress_address = stress_address, user_city = city, dob = dob)
                new_user.save()
                user = User.objects.create_user(name, email, rawPassword)
                user.save()
                subject = 'Thank you for your registration'
                message = 'Welcome to Car Rental'
                from_email = settings.EMAIL_HOST_USER
                to_list = [new_user.email]
                send_mail(subject, message, from_email, to_list, fail_silently = False)
                success = 'Thank you for your registration!\nCheck your mail box to verify!\nYou can log in now'
                return render(request, 'rentals/thank.html', {'tag1': 'Login', 'tag2':'Register', 'success': success})
    else: form = Register()
    return render(request, 'rentals/create_account.html', {'form': form, 'tag1': 'Login', 'tag2':'Register'})

def result(request):
    return render(request, 'rentals/result.html')
    
@login_required(login_url='/admin/login/?next=/admin/')
def edit_customer(request):
    labels = ["ID","Name", "Phone Number", "Address","Birthday","Occupation", "Gender"]
    customers = Customer.objects.values_list()
    return render(request, 'rentals/edit_customer.html',{ 'customers':customers, 'labels':labels,'tag1': 'Login', 'tag2':'Register'})

@login_required(login_url='/admin/login/?next=/admin/')
def edit_car(request):
    labels = ["ID","Make Name", "Model", "Series", "Series Year","Price New","Engine Size","Fuel System","Tank Capacity","Power","Seating Capacity","Standard Transmission","Bodytype","Drive","Wheel Base"]
    data = Car.objects.values_list()
    return render(request, 'rentals/edit_car.html',{ 'data':data, 'labels':labels,'tag1': 'Login', 'tag2':'Register'})

@login_required(login_url='/admin/login/?next=/admin/')
def edit_order(request):
    labels = ["ID","Create Date", "Pickup Date", "Pickup Store ID", "Return Store ID","Return Date", "Car ID", "Customer ID"]
    data = Order.objects.values_list()
    return render(request, 'rentals/edit_order.html',{ 'data':data, 'labels':labels,'tag1': 'Login', 'tag2':'Register'})

@login_required(login_url='/admin/login/?next=/admin/')
def edit_store(request):
    labels = ["ID","Name", "State", "City","Address","Phone Number"]
    data = Store.objects.values_list()
    return render(request, 'rentals/edit_store.html',{ 'data':data, 'labels':labels,'tag1': 'Login', 'tag2':'Register'})

@login_required(login_url='/admin/login/?next=/admin/')
def staff(request):
    labels = ["Customers","Cars", "Orders", "Stores"]
    customers = Order.objects.count()
    cars = Car.objects.count()
    orders = Order.objects.count()
    stores = Store.objects.count()
    return render(request, 'rentals/staff.html',{ 'stafforcust': 'Customer Site','labels':labels,'customers':customers, 'cars':cars,'orders': orders, 'stores':stores,'tag1': 'Login', 'tag2':'Register'})

	
def recommendation(request):
    return render(request, 'rentals/recommendation.html')
	
def login(request):
    if request.method == 'POST':
        form = Login(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            name = data['user_name']
            rawPassword = data['password']
            password = hashlib.md5(rawPassword.encode('utf-8')).hexdigest()
            # isValidUser = user_authenticate(name, password)
            # isValidUser = user.is_authenticated
            user = authenticate(username=name, password=rawPassword)
            try:
                if user is None:
                    raise ValueError('Incorrect Login Details. Please enter correct entries.')
            except ValueError as error:
                context = {
                    "form": form,
                    "error_message": error.args[0],
                }
                return render(request, 'rentals/login.html', context)
            else:
                form = Search()
                auth_login(request, user)
                # return render(request, 'rentals/user_index.html', {'form': form, 'tag1': 'Hi ' + name, 'tag2':'Logout'})   
                if (request.user.is_superuser):
                    return redirect('staff_account')
                else:          
                    return redirect('home')
                
    else: form = Login()
    return render(request, 'rentals/login.html', {'form': form, 'tag1': 'Login', 'tag2':'Register'})
def user_authenticate(username, password):
    if Newaccount.objects.filter(user_name=username).exists():
        querySet = Newaccount.objects.get(user_name=username)
        if (querySet.password == password):
            return True
    return False

def home(request):
    form = Search()
    season = 0
    month = datetime.datetime.today().month
    if month in (1, 2, 3):
        most_rented = Order.objects.filter(Q(order_pickupdate__month= 1)|Q(order_pickupdate__month= 2)|Q(order_pickupdate__month= 3)).values_list('car').annotate(car_count=Count('car')).order_by('-car_count')[:6]
        season = 'winter'
    elif month in (4, 5, 6):
        most_rented = Order.objects.filter(Q(order_pickupdate__month= 4)|Q(order_pickupdate__month= 5)|Q(order_pickupdate__month= 6)).values_list('car').annotate(car_count=Count('car')).order_by('-car_count')[:6]
        season = 'spring'
    elif month in (7, 8, 9):
        most_rented = Order.objects.filter(Q(order_pickupdate__month= 7)|Q(order_pickupdate__month= 8)|Q(order_pickupdate__month= 9)).values_list('car').annotate(car_count=Count('car')).order_by('-car_count')[:6]
        season = 'summer'
    else:
        most_rented = Order.objects.filter(Q(order_pickupdate__month= 10)|Q(order_pickupdate__month= 11)|Q(order_pickupdate__month= 12)).values_list('car').annotate(car_count=Count('car')).order_by('-car_count')[:6]
        season = 'autumn'
    Qdata = list(most_rented)
    IDs = [list(elem) for elem in Qdata]
    for x in range(6):
        del IDs[x][1]
    IDs = sum(IDs, [])
    cars = Car.objects.filter(pk__in=IDs)
    if (request.user.is_superuser):
        return render(request, 'rentals/staff.html', {'stafforcust': 'Customer Site'})
    if(request.user.is_authenticated):
        return render(request, 'rentals/user_index.html', {'IDs':IDs,'most_rented':most_rented,'cars': cars, 'form': form})

    return render(request, 'rentals/user_index.html', {'cars': cars, 'form': form})

def profile(request):
    account = Newaccount.objects.get(user_name=request.user.username)
    return render(request, 'rentals/user_profile.html', {'account': account})

def edit_profile(request):
    user = request.user
    account = Newaccount.objects.get(user_name=request.user.username)
    if request.method == 'POST':
        form = Edit_Profile(request.POST)
        if form.is_valid():
            if form.cleaned_data['email']:
                account.email = form.cleaned_data['email']
                account.save(update_fields=['email'])
            if form.cleaned_data['phone_number']:
                account.phone_number = form.cleaned_data['phone_number']
                account.save(update_fields=['phone_number'])
            if form.cleaned_data['dob']:
                account.dob = form.cleaned_data['dob']
                account.save(update_fields=['dob'])
            if form.cleaned_data['stress_address']:
                account.stress_address = form.cleaned_data['stress_address']
                account.save(update_fields=['stress_address'])
            if form.cleaned_data['user_city']:
                account.user_city = form.cleaned_data['user_city']
                account.save(update_fields=['user_city'])
            alert = "Updated successfully!!!"
            return render(request, 'rentals/user_profile.html', {'alert': alert, 'account': account})
    else:
        form = Edit_Profile()
        return render(request, 'rentals/user_edit.html', {'account': account, 'form': form})

def logout_view(request):
    message = "Logged out successfully!!!"
    logout(request)
    form = Search()
    return render(request, 'rentals/index.html', {'form': form, 'tag1': 'Login', 'tag2':'Register', 'message': message})

@login_required(login_url='/admin/login/?next=/admin/')
def data_report_car(request):
    labels = ["Car ID","Times rented"]
    orders = Order.objects.all()
    most_rented = Order.objects.values_list('car').annotate(car_count=Count('car')).order_by('-car_count')
    return render(request, 'rentals/data_report_car.html', { 'orders': orders, 'tag1': 'Login', 'tag2':'Register', 'most_rented': most_rented, 'labels':labels})

@login_required(login_url='/admin/login/?next=/admin/')
def data_report_type(request):
    labels = ["Car make name","Total of makes"]
    orders = Order.objects.all()
    most_rented = Order.objects.values_list('car__car_makename').annotate(car_count=Count('car__car_makename')).order_by('-car_count')
    return render(request, 'rentals/data_report_type.html', { 'orders': orders, 'tag1': 'Login', 'tag2':'Register', 'most_rented': most_rented, 'labels':labels})
   
@login_required(login_url='/admin/login/?next=/admin/')
def data_report_location(request):
    labels = ["Store ID","Times rented from store"]
    orders = Order.objects.all()
    most_rented = Order.objects.values_list('order_pickupstore').annotate(car_count=Count('order_pickupstore')).order_by('-car_count')
    return render(request, 'rentals/data_report_location.html', { 'orders': orders, 'tag1': 'Login', 'tag2':'Register', 'most_rented': most_rented, 'labels':labels})

def history(request):
    if request.method == 'POST':
        form = History(request.POST)
        labels = ["Order ID", "Date Picked up", "Date Returned","Car ID"]
        customerhistory = request.POST.get('CUSTOMERHISTORY')
        customer_history = Order.objects.filter(customer = customerhistory)
        return render(request, 'rentals/history.html', {'form': form, 'tag1': 'Login', 'tag2':'Register', 'customer_history':customer_history,'labels':labels})

    else:
        form = History()
    return render(request, 'rentals/staff.html', {'form': form, 'tag1': 'Login', 'tag2':'Register'})

def new_customer(request):
    form = NewCustomer()
    if request.method == 'POST':
        form = NewCustomer(request.POST)
        if form.is_valid():
            cust_id = random.randrange(11000, 15000)
            while Customer.objects.filter(customer_id = cust_id).exists():
                cust_id = random.randrange(11000, 15000)
            data = form.cleaned_data
            name = data['customer_name']
            phone = data['customer_phone']
            address = data['customer_address']
            birthday = data['customer_birthday']
            occupation = data['customer_occupation']
            gender = data['customer_gender']
            new_customer = Customer(customer_id = cust_id,customer_name = name, customer_phone = phone, customer_address = address, customer_birthday = birthday, customer_occupation = occupation, customer_gender = gender)
            new_customer.save()
            form = Pickup({'customer': cust_id})
            
            return render(request, 'rentals/pickup_car.html', {'form': form})
            
    else: 
        form = NewCustomer()
        return render(request, 'rentals/new_customer.html', {'form': form})

def pickup_car(request):
    form = Pickup()
    if request.method == 'POST':
        form = Pickup(request.POST or None)
        if form.is_valid():
            order_num = random.randrange(1, 10000)
            while Order.objects.filter(order_id = order_num).exists():
                order_num = random.randrange(1, 10000)
            data = form.cleaned_data               
            pickup_store = data['order_pickupstore']
            car_id = data['car']
            customer_id = data['customer']
            create_date = datetime.datetime.now().date()
            pickup_date = create_date

                                       
            
            new_order = Order(order_id = order_num, order_createdate = create_date, order_pickupdate = create_date, order_pickupstore = pickup_store, car = car_id, customer = customer_id)
            new_order.save()
            
            messages.add_message(request, messages.INFO, 'The database has been successfully updated.')
            return HttpResponseRedirect('/staff/')    
            msg = 'The database has been successfully updated.'
            return render(request, 'rentals/staff.html', {'form': form, 'message': msg})
            
         
    else: 
        form = Pickup()
        return render(request, 'rentals/pickup_car.html', {'form': form})

def return_car(request):
    form = ReturnCar()
    if request.method == 'POST':
        form = ReturnCar(request.POST or None)
        if form.is_valid():
            
            data = form.cleaned_data   
            lookup_id = data['order_id']

            store_id_num = data['store_id']
            
            if not Order.objects.filter(order_id = lookup_id).exists():
                messages.add_message(request, messages.INFO, 'Sorry, that order does not exist')
                return HttpResponseRedirect('/staff/')
                msg = 'This order does not exist.'   
                return render(request, 'rentals/staff.html', {'form': form, 'message': msg})
                
            elif not Store.objects.filter(order_id = lookup_id).exists():
                messages.add_message(request, messages.INFO, 'Sorry, that store does not exist')
                return HttpResponseRedirect('/staff/')
                msg = 'This store does not exist.'      
                return render(request, 'rentals/staff.html', {'form': form, 'message': msg})
                
            else:
                
                current_order = Order.objects.get(order_id = lookup_id)
                current_order.order_returndate = datetime.datetime.now().date()
                current_store = Store.objects.get(store_id = store_id_num)
                current_order.order_returnstore = current_store
                current_order.save()
                messages.add_message(request, messages.INFO, 'The database has been successfully updated.')                 
                return HttpResponseRedirect('/staff/')
                msg = 'The database has been successfully updated.'  
                return render(request, 'rentals/staff.html', {'form': form, 'message': msg})
                
    else:
        form = ReturnCar()
        return render(request, 'rentals/return_car.html', {'form': form})

	
def car_detail(request, id):
    try:
        car = Car.objects.get(car_id=id)
    except Car.DoesNotExist:
        raise Http404('Car not found')
    return render(request, 'rentals/car_detail.html', {'car': car, 'tag1': 'Login', 'tag2':'Register'})
	
def user_car_detail(request, id):
    try:
        car = Car.objects.get(car_id=id)
    except Car.DoesNotExist:
        raise Http404('Car not found')
    return render(request, 'rentals/user_car_detail.html', {'car': car, 'tag1': 'My account', 'tag2':'Logout'})



def default_map(request):
    return render(request, 'rentals/maps.html', {'tag1': 'Login', 'tag2': 'Register'})
def user_maps(request):
    return render(request, 'rentals/user_maps.html')

def delete_account(request):
    user = request.user
    user.is_active = False
    user.save()
    messages.success(request, 'Profile successfully disabled.')
    return redirect('index')

