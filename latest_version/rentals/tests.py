from django.test import TestCase, Client
from django.contrib.auth.models import User
from .models import *

from .forms import Register, Search
from django.urls import reverse
import hashlib


# Create your tests here.
class AccountTestCase(TestCase):
    def setUp(self):
        rawPassword = '123'
        password = hashlib.md5(rawPassword.encode('utf-8')).hexdigest()
        Newaccount.objects.create(user_name = 'a', password = password, email = 'a@gamil.com', phone_number = '09238456', dob = '1900-01-01', stress_address = '0', user_city = 'A')

    def testUsernameFields(self):
        user = Newaccount.objects.get(user_name = 'a')
        self.assertEqual(user.email, 'a@gamil.com') # should return True
        self.assertFalse(user.phone_number == 'a@gamil.com') # should return False

    def testExistUser(self):
        self.assertTrue(Newaccount.objects.filter(user_name='a').exists()) # should return True

class LogInTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser', is_active=1)
        user.set_password('12345')
        user.save()

    def test_login(self):
        c = Client()

        response = c.get('/login')
        self.assertEqual(response.status_code, 200)

        logged_in1 = c.login(username='testuser', password='12345')
        self.assertTrue(logged_in1) # should return True

        logged_in2 = c.login(username='testuser', password='1dddd23445')
        self.assertFalse(logged_in2) # should return False

class LogOutTest(TestCase):
    def test_logout(self):
        # # Log in
        self.client.login(username='XXX', password="XXX")

        # Check response code
        response = self.client.get('/login')
        self.assertEquals(response.status_code, 200)

        # # Log out
        self.client.logout()

        # Check response code
        response = self.client.get('/logout/')
        self.assertEquals(response.status_code, 200)

class RegistrationViewTestCase(TestCase):
    def test_registration_view_get(self):
        response = self.client.get(reverse('create_account'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'rentals/create_account.html')
        self.failUnless(isinstance(response.context['form'], Register))
    def test_invalid_form(self):
        data1 = {'name': 'aaa', 'uppemailer': 'a', 'stress_address': '',}
        form = Register(data = data1)
        self.assertFalse(form.is_valid()) # should be False

class SearchFunctionTest(TestCase):
    def test_valid_form(self):
        data1 = {'lower': 1, 'upper': 100000, 'brand': 'BMW',}
        form = Search(data = data1)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.data['lower'], 1) #should be True
