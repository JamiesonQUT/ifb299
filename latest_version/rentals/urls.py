from django.conf.urls import url
from django.contrib import admin
from django.conf import settings 
from django.contrib.auth import views as auth_views

from rentals import views
admin.site.site_header = "Service car rental Admin"
admin.site.site_title = "Service car rental Portal"
admin.site.index_title = "Welcome to Service car rental Portal"

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^search', views.search, name='search'),
    url(r'^result', views.result, name='result'),
    url(r'^recommendation', views.recommendation, name='recommendation'),
    url(r'^login', views.login, name='login'),
    url(r'^logout', views.logout_view, name='logout'),
    url(r'^data_report_car', views.data_report_car, name='data_report_car'),
    url(r'^data_report_type', views.data_report_type, name='data_report_type'),
    url(r'^data_report_location', views.data_report_location, name='data_report_location'),
    url(r'^pickup_car', views.pickup_car, name='pickup_car'),
    url(r'^return_car', views.return_car, name='return_car'),
    url(r'^new_customer', views.new_customer, name='new_customer'),
    url(r'^cars_detail/(\d+)/', views.car_detail, name='car_detail'),
    url(r'^user_cars_detail/(\d+)/', views.user_car_detail, name='user_car_detail'),
    url(r'^create_account', views.create_account, name='create_account'),
    url(r'^profiles/home', views.home, name='home'),
    url(r'^profile', views.profile, name='profile'),
    url(r'^edit_profile', views.edit_profile, name='edit_profile'),
    url(r'^user_search', views.user_search, name='user_search'),
    url(r'^staff', views.staff, name='staff_account'),
    url(r'^history', views.history, name='history'),
    url(r'^maps', views.default_map, name="maps"),
    url(r'^user_maps', views.user_maps, name="user_maps"),
    url(r'^edit_customer', views.edit_customer, name='edit_customer'),
    url(r'^edit_car', views.edit_car, name='edit_car'),
    url(r'^edit_order', views.edit_order, name='edit_order'),
    url(r'^edit_store', views.edit_store, name='edit_store'),
    url(r'^delete-account', views.delete_account, name ='delete_account'),
    
    url(r'^password-reset$', auth_views.PasswordResetView.as_view(template_name='rentals/password_reset_form.html'), name = 'password_reset'),
    
    url(r'^password-reset/done$', auth_views.PasswordResetDoneView.as_view(template_name='rentals/password_reset_done.html'), name = 'password_reset_done'),
    
    url(r'^password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.PasswordResetConfirmView.as_view(template_name='rentals/password_reset_confirm.html'), name = 'password_reset_confirm'), 
   
    url(r'^password-reset-complete$', auth_views.PasswordResetCompleteView.as_view(template_name='rentals/password_reset_complete.html'), name = 'password_reset_complete'),

]
