

STORE
INSERT INTO rentals_store(store_id, store_name, store_state,city, address, phone_number)
SELECT Order_PickupStore, Return_Store_Name, Return_Store_State, Return_Store_City, Return_Store_Address, Return_Store_Phone FROM dataincentraldatabase GROUP BY Order_PickupStore;




CUSTOMER
INSERT INTO rentals_customer(customer_id, customer_name, customer_phone, customer_address, customer_birthday, customer_occupation, customer_gender)
SELECT Customer_ID, Customer_Name, Customer_Phone, Customer_Addresss, Customer_Brithday, Customer_Occupation, Customer_Gender FROM dataincentraldatabase GROUP BY Customer_ID;




CAR
INSERT INTO rentals_car(car_id, car_makename, car_model, car_series, car_fuelsystem, car_standardtransmission, car_bodytype, car_drive, car_seriesyear, car_enginesize, car_power, car_pricenew, car_seatingcapacity, car_tankcapacity, car_wheelbase)
SELECT Car_ID, Car_MakeName, Car_Model, Car_Series, Car_FuelSystem, Car_StandardTransmission, Car_BodyType, Car_Drive, Car_SeriesYear, Car_EngineSize, Car_Power, Car_PriceNew, Car_SeatingCapacity, Car_TankCapacity, Car_Wheelbase FROM dataincentraldatabase GROUP BY Car_ID;




ORDER
INSERT INTO rentals_order(order_id, order_createdate, order_pickupdate, order_returndate, car_id, customer_id, order_pickupstore_id, order_returnstore_id)
SELECT Order_ID, Order_CreateDate, Order_PickupDate, Order_ReturnDate, Car_ID, Customer_ID, Order_PickupStore, Order_ReturnStore FROM dataincentraldatabase GROUP BY Order_ID;


