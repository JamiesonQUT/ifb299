from django.contrib import admin

## Register your models here.
from .models import Car
from .models import Customer
from .models import Store
from .models import Order
admin.site.register(Car)
admin.site.register(Customer)
admin.site.register(Store)
admin.site.register(Order)