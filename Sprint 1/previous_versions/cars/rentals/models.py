from django.db import models



class Car(models.Model):

	car_id = models.IntegerField(primary_key = True)

	car_makename = models.CharField(max_length = 20)

	car_model = models.CharField(max_length = 50)

	car_series = models.CharField(max_length = 50, null = True, blank = True)

	car_seriesyear = models.IntegerField(default= 0)

	car_pricenew = models.IntegerField(default= 0)

	car_enginesize = models.CharField(max_length = 5, default = 0)

	car_fuelsystem = models.CharField(max_length = 25, default = 0)

	car_tankcapacity = models.CharField(max_length = 5, default = 0)

	car_power = models.CharField(max_length = 5, default = 0)

	car_seatingcapacity = models.IntegerField(default = 5)

	car_standardtransmission = models.CharField(max_length = 10, default = 0)

	car_bodytype = models.CharField(max_length = 50, default = 0)

	car_drive = models.CharField(max_length = 10, default = 0)

	car_wheelbase = models.CharField(max_length = 6, default = 0)



class Customer(models.Model):

        GENDER_CHOICES = [('M ','Male'),('F ','Female')]

        customer_id = models.IntegerField(primary_key = True)

        customer_name = models.CharField(max_length = 50)

        customer_phone = models.CharField(max_length = 30)

        customer_address = models.CharField(max_length = 50)

        customer_birthday = models.DateField(auto_now_add=False)

        customer_occupation = models.CharField(max_length = 50)

        customer_gender = models.CharField(max_length=1,choices=GENDER_CHOICES)



class Store(models.Model):

        store_id = models.IntegerField(primary_key = True)

        store_name = models.CharField(max_length = 50)

        store_state = models.CharField(max_length = 50)

        city = models.CharField(max_length = 50)

        address = models.CharField(max_length = 50)

        phone_number = models.CharField(max_length = 30)



class Order(models.Model):

        order_id = models.IntegerField(primary_key = True)

        order_createdate = models.DateField(auto_now_add=False)

        order_pickupdate = models.DateField(auto_now_add=False)

        order_pickupstore = models.ForeignKey(Store, on_delete=models.CASCADE, related_name='Stores_requests_created')

        order_returnstore = models.ForeignKey(Store, on_delete=models.CASCADE)

        order_returndate = models.DateField(auto_now_add=False)

        order_returndate = models.DateField(auto_now_add=False)

        car = models.ForeignKey(Car, on_delete=models.CASCADE)

        customer = models.ForeignKey(Customer, on_delete=models.CASCADE)