# Generated by Django 2.1.1 on 2018-09-18 06:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rentals', '0007_auto_20180918_1445'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='customer_gender',
            field=models.CharField(choices=[('M ', 'Male'), ('F ', 'Female')], max_length=1),
        ),
    ]
