from django.shortcuts import render
from django.http import HttpResponse
from django.http import Http404

from .models import Car
from .models import Customer
from .models import Store
from .models import Order

def index(request):
    cars = Car.objects.all()
    return render(request, 'rentals/index.html', {'cars': cars})
	
def search(request):
    return render(request, 'rentals/search.html')
	
def result(request):
    return render(request, 'rentals/result.html')
	
def recommendation(request):
    return render(request, 'rentals/recommendation.html')
	
def login(request):
    return render(request, 'rentals/login.html')

def logout(request):
    return render(request, 'rentals/logout.html')
	
def data_report(request):
    return render(request, 'rentals/data_report.html')

def pickup_return(request):
    return render(request, 'rentals/pickup_return.html')


def pickup_car(request):
    return render(request, 'rentals/pickup_car.html')

def return_car(request):
    return render(request, 'rentals/return_car.html')










#def pickup_car(request, id):
#    try:
#        order = Order.objects.get(order_id=id)
#    except Order.DoesNotExist:
#        raise Http404('Order not found')
#    return render(request, 'rentals/pickup_car.html', {'order': order})
#
#def return_car(request, id):
#    try:
#        order = Order.objects.get(order_id=id)
#    except Order.DoesNotExist:
#        raise Http404('Order not found')
#    return render(request, 'rentals/return_car.html', {'order': order})
	
def car_detail(request, id):
    try:
        car = Car.objects.get(car_id=id)
    except Car.DoesNotExist:
        raise Http404('Car not found')
    return render(request, 'rentals/car_detail.html', {'car': car})

def create_account(request):
    return render(request, 'rentals/create_account.html')
	
def customer_account(request, id):
    try:
        customer = Customer.objects.get(customer_id=id)
    except Customer.DoesNotExist:
        raise Http404('Account not found')
    return render(request, 'rentals/customer_account.html', {'customer': customer})
	
# Defining a function to perform user authentication
# @param username The username submitted in the login form
# @param password The password submitted in the login form
# @param accountNumber The Account Number submitted in the login form
# @returns A boolean based on whether the submitted login credential matches the entries from the database


#def authenticate(username, password, accountID):
#    try:
#        if NEWACCOUNT.objects.filter(ACCOUNTNO=accountID).exists():
#            querySet = NEWACCOUNT.objects.get(ACCOUNTNO = accountID)
#            if ((querySet.USERNAME != username) or (querySet.PASSWORD != password)):
#                return False
#        else:
#            return False
#    except ValueError as error:
#        return False
#    else:
#        return True
#	
#def staff_account(request, id):
#    try:
#        staff = Store.objects.get(id=id)
#    except Store.DoesNotExist:
#        raise Http404('Store not found')
#    return render(request, 'staff_account.html', {'staff': staff})