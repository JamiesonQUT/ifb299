from django.conf.urls import url
from django.contrib import admin

from rentals import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
	url(r'^search', views.search, name='search'),
	url(r'^result', views.result, name='result'),
	url(r'^recommendation', views.recommendation, name='recommendation'),
	url(r'^login', views.login, name='login'),
	url(r'^logout', views.logout, name='logout'),
	url(r'^data_report', views.data_report, name='data_report'),
    url(r'^pickup_car/', views.pickup_car, name='pickup_car'),
	url(r'^pickup_return', views.pickup_return, name='pickup_return'),
	url(r'^return_car/', views.return_car, name='return_car'),
	url(r'^cars/(\d+)/', views.car_detail, name='car_detail'),
	url(r'^create_account', views.create_account, name='create_account'),
#	url(r'^customer/(\d+)/', views.customer, name='customer_account'),
#	url(r'^staff/(\d+)/', views.staff, name='staff_account'),
]
