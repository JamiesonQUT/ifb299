import pandas as pd

import xlsxwriter

file_name = "Copy of CarRentalDataSourceM.xlsx"

output_file_name = "Cleaned_" + file_name



wrong_addresses = ['5, avenue de la Gare', '4, impasse Ste-Madeleine', 'o?str 5538', 'eiter Weg 7765', 'ostenweg 2428', '205, rue Malar',

                   'ostfach 99 92 92', ', rue de Linois', '33, rue de Linois', 'otth?user Weg 636', 'eiderplatz 662', '8, avenue de l? Union Centrale',

                    '1, rue de la Cavalerie', 'Dunckerstr 22525', 'Zeiter Weg 7765', '8205, rue Malar', '268, avenue de l?Europe', 'Rotth?user Weg 636','Ro?str 5538', '88, avenue de l? Union Centrale', 'Postfach 99 92 92' ]



## correct DataInStore

data1 = pd.read_excel(file_name, sheet_name = 0, converters = {'Car_Model': str, 'Customer_Brithday': str})

## delete row which contains "NULL"

data1.dropna(axis = 0, how = 'any', inplace = True)

## Correct store name

data1.Store_Name.replace('Alexandria_stroe', 'Alexandria_store', inplace=True)

data1.Car_Model.replace(['2012-03-09 00:00:00', '2012-05-09 00:00:00'], 'Unknown', inplace=True)

## delete rows containing wrong address

data1 = data1[~data1.Customer_Addresss.isin(wrong_addresses)]



## Correct DataInCentralDatabase

data2 = pd.read_excel(file_name, sheet_name = 1, converters = {'Car_Model': str, 'Customer_Brithday': str})

## delete row which contains "NULL"

data2.dropna(axis = 0, how = 'any', inplace = True)

## Correct store name

data2.Pickup_Store_Name.replace('Alexandria_stroe', 'Alexandria_store', inplace=True)

data2.Return_Store_Name.replace('Alexandria_stroe', 'Alexandria_store', inplace=True)

## delete rows containing wrong address

data2 = data2[~data2.Customer_Addresss.isin(wrong_addresses)]

data2.Car_Model.replace(['2012-03-09 00:00:00', '2012-05-09 00:00:00'], 'unknown', inplace = True)

data2 = data2[data2.Car_Model != '(4x4)']

data2.Car_Series = data2.Car_Series.str.replace('4x4', '')

data2.Car_StandardTransmission = data2.Car_StandardTransmission.str.replace('4x4', ' ')

data2.Customer_Phone = data2.Customer_Phone.str.replace('*', '')



# Create a Pandas Excel writer
##writer = pd.ExcelWriter(output_file_name, engine='xlsxwriter')

data1.to_excel('dataInstore.xlsx', engine='xlsxwriter')

data2.to_excel('dataInCentralDatabase.xlsx', engine='xlsxwriter')

##workbook = writer.book
##
##writer.save()

